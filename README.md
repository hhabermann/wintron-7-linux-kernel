# Wintron 7 Linux kernel

This is the vanilla Linux 5.2.5 kernel and kenel configuration to make it run on Wintron 7

## Compile

* Install (text mode) Debian 10 with *i386* architecture in a virtual machine with the same amount of virtual CPUs you have physical CPUs in your PC
* Install the packages (git build-essential bc kmod cpio flex cpio libncurses5-dev) needed to compile kernels
* Clone this repository
* Go to the new directory
* Compile with: make -j`nproc` bindeb-pkg
* This should create Debian packages (.deb) that can be installed in Wintron 7 running Debian 10

## Your tasks

* Fork this project :-)
* Use the configuration with never kernel versions and optimise it
* Try to get additional hardware of the Wintron 7 to work: LCD backlight adjustment, battery status, etc